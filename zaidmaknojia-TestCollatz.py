#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 1)

    def test_read_3(self):
        s = "20 20\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  20)
        self.assertEqual(j, 20)
    
    def test_read_4(self):
        s = "71 72\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  71)
        self.assertEqual(j, 72)
    
    def test_read_5(self):
        s = "1023 7621\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1023)
        self.assertEqual(j, 7621)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)
    
    def test_eval_5(self):
        v = collatz_eval(319662, 322868)
        self.assertEqual(v, 371)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 900, 1000, 174)
        self.assertEqual(w.getvalue(), "900 1000 174\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 605293, 606510, 341)
        self.assertEqual(w.getvalue(), "605293 606510 341\n")

    def test_print_5(self):
        w = StringIO()
        collatz_print(w, 948351, 948681, 326)
        self.assertEqual(w.getvalue(), "948351 948681 326\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")


    def test_solve_2(self):
        r = StringIO("200 200\n4000 9999\n543 546\n30 31\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "200 200 27\n4000 9999 262\n543 546 137\n30 31 107\n")


    def test_solve_3(self):
        r = StringIO("50 12\n10000 1\n42 19\n25 7\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "50 12 112\n10000 1 262\n42 19 112\n25 7 24\n")

    def test_solve_4(self):
        r = StringIO("478642 479101\n815892 815933\n719220 719135\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "478642 479101 338\n815892 815933 269\n719220 719135 274\n")

    def test_solve_5(self):
        r = StringIO("532347 536933\n545105 545167\n850670 851113\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "532347 536933 377\n545105 545167 209\n850670 851113 357\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
